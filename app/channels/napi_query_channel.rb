class NapiQueryChannel < ApplicationCable::Channel
  def subscribed
    puts "subscribed! query_id=#{params[:query_id]}"
    napi_channel_name = "Napi_Query_Channel_#{params[:query_id]}"
    stream_from napi_channel_name
    ActionCable.server.broadcast napi_channel_name, {
        type: 'channel_subscription',
        query_id: params[:query_id]
    }
  end

  def unsubscribed
    puts "unsubscribed! query_id=#{params[:query_id]}"
    napi_channel_name = "Napi_Query_Channel_#{params[:query_id]}"
    ActionCable.server.broadcast napi_channel_name, {
        type: 'channel_unsubscription',
        query_id: params[:query_id]
    }
  end
end