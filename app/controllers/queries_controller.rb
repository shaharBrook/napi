class QueriesController < ApplicationController

  def update_count
    query_id, count = params[:query_id], params[:count]
    napi_channel_name = "Napi_Query_Channel_#{query_id}"
    ActionCable.server.broadcast napi_channel_name, {
        type: 'channel_count_update',
        query_id: params[:query_id],
        count: count
    }
    render json: { query_id: query_id, count: count }
  end

end
