Rails.application.routes.draw do
  mount ActionCable.server => '/cable'

  post '/queries/:query_id/count/:count', to: 'queries#update_count'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
